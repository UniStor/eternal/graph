# graph

# [RHash](https://github.com/rhash/RHash)

"rhash with -rc does more or less the same thing as cfv, performance is good and it supports most hash formats including bittorrent. It lacks cfv's -m though."
— [amazingdash commented on May 17](https://github.com/cfv-project/cfv/issues/8#issuecomment-629828797)

# Alt:
## Hashdeep
